/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.db.javademo.bean;

/**
 *
 * @author ThucNV
 */
public class User {

    private int id;
    private String name;
    private String email;
    private String pasword;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }

    public User() {
    }

    public User(int id, String name, String email, String pasword) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.pasword = pasword;
    }

    public User(String name, String email, String pasword) {
        this.name = name;
        this.email = email;
        this.pasword = pasword;
    }

}

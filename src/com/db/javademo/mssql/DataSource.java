package com.db.javademo.mssql;

import com.db.config.AppConfig;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

/**
 *
 * @author ThucNV
 */
public class DataSource {

    // <editor-fold defaultstate="collapsed" desc=" Database method ">
    //Insert Query
    /**
     * Insert new record to database
     *
     * @param tableName Name of table
     * @param KeyValue HashMap(String feildName, String values). value
     * @return Number of record inserted
     * @throws ClassNotFoundException SQLdrever
     * @throws SQLException
     */
    public int insertCommand(String tableName, HashMap<String, String> KeyValue) throws ClassNotFoundException, SQLException {
        String feilds = "";
        String values = "";
        for (String key : KeyValue.keySet()) {
            feilds += "[" + key + "],";
            values += "?" + ",";
        }
        feilds = feilds.substring(0, feilds.length() - 1);
        values = values.substring(0, values.length() - 1);
        String strQuery = "INSERT INTO [" + tableName + "](" + feilds + ") VALUES(" + values + ")";
        PreparedStatement psSQL = getConnection().prepareStatement(strQuery);
        int i = 1;
        for (String para : KeyValue.keySet()) {
            psSQL.setString(i, KeyValue.get(para));
            i++;
        }
        return psSQL.executeUpdate();
    }

    //Update Command
    /**
     *
     * @param tableName
     * @param KeyValue
     * @param ID
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public int updateCommand(String tableName, HashMap<String, String> KeyValue, int ID) throws ClassNotFoundException, SQLException {
        String feilds = "";
        String id = "Id =" + ID;
        for (String key : KeyValue.keySet()) {
            feilds += " [" + key + "] = ?,";
        }
        feilds = feilds.substring(0, feilds.length() - 1);
        String strQuery = "UPDATE [" + tableName + "] SET " + feilds + " WHERE " + id;
        // return strQuery;
        PreparedStatement psSQL = getConnection().prepareStatement(strQuery);
        int i = 1;
        for (String key : KeyValue.keySet()) {
            psSQL.setString(i, KeyValue.get(key));
            i++;
        }
        return psSQL.executeUpdate();
    }

    //DELETE command
    public int deleteCommand(String tableName, int ID) throws ClassNotFoundException, SQLException {
        String sql = "DELETE FROM [" + tableName + "] WHERE Id = ?";
        PreparedStatement psSQL = getConnection().prepareStatement(sql);
        psSQL.setInt(1, ID);
        return psSQL.executeUpdate();
    }

    /**
     * Delete list record by id
     *
     * @param tableName name of table
     * @param arrID ArrayList id of record
     * @return number of record deleted
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public int deleteCommand(String tableName, int[] arrID) throws ClassNotFoundException, SQLException {
        String lstId = "";
        for (int id : arrID) {
            lstId += " " + id + ",";
        }
        lstId = lstId.substring(0, lstId.length() - 1);
        String sql = "DELETE FROM [" + tableName + "] WHERE Id IN (" + lstId + ")";
        return execNonQuery(sql);
    }

    /**
     *
     * @param table table name
     * @param feilds feild to select
     * @param wheres condition to select
     * @param order feild to arder
     * @param descOrder true - desc, false - asc
     * @return
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public ResultSet selectCommand(String table, Vector<String> feilds, HashMap<String, String> wheres, String order, boolean descOrder) throws ClassNotFoundException, SQLException {

        String sql = "SELECT";
        if (feilds == null || feilds.isEmpty()) {
            sql += " *,";
        } else {
            for (String feild : feilds) {
                sql += " " + feild + ",";
            }
        }
        sql = sql.substring(0, sql.length() - 1);

        sql += " FROM [" + table + "]";
        if (wheres != null && !wheres.isEmpty()) {
            sql += " WHERE ";
            for (String fei : wheres.keySet()) {
                sql += " [" + fei + "] = " + wheres.get(fei) + ",";
            }
            sql = sql.substring(0, sql.length() - 1);
        }

        if (order != null && !order.equals("")) {
            sql += " ORDER BY [" + order + "] ";
            if (descOrder) {
                sql += " DESC";
            }
        }

        return execQuery(sql);
    }

    public ResultSet filterCommand(String table, String feild, String keyword, boolean order) throws ClassNotFoundException, SQLException {

        //<editor-fold defaultstate="collapsed" desc="Build sql query">
        //Query string
        String sql = "select * from [" + table + "] where [" + feild + "] like ? order by [" + feild + "]";

        if (order) {
            sql += " desc";
        }
        //</editor-fold>
        //<editor-fold defaultstate="collapsed" desc="Set Parameter">
        PreparedStatement ps = getConnection().prepareStatement(sql);
        ps.setString(1, "%" + keyword + "%");
        //</editor-fold>
        return ps.executeQuery();
    }

    //Get Connection
    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Connection result = null;
        String strConn
                = "jdbc:" + cf.getDbType()
                + "://" + cf.getdbHost() + ";"
                + "databaseName=" + cf.getDbName();
        Class.forName(cf.getDbDriver());
        result = DriverManager.getConnection(strConn, cf.getDbUser(), cf.getDbPassword());
        return result;
    }

    //Check config
    public boolean checkConfig() {
        try {
            Connection conn = getConnection();
            if (conn == null) {
                return false;
            }
            return true;
        } catch (ClassNotFoundException | SQLException ex) {
            return false;
        }

    }

    //Excec Query
    public ResultSet execQuery(String sql) throws ClassNotFoundException, SQLException {
        return getConnection().createStatement().executeQuery(sql);
    }

    //Ecxec non-query
    public int execNonQuery(String sql) throws ClassNotFoundException, SQLException {
        return getConnection().createStatement().executeUpdate(sql);
    }

    public ResultSet execProc(String procName, String[] params) throws ClassNotFoundException, SQLException {
        ResultSet rs = null;
        CallableStatement cs = this.getConnection().prepareCall(procName);
        for (int i = 0; i < params.length; i++) {
            cs.setString(i + 1, params[i]);
        }
        rs = cs.executeQuery();
        return rs;
    }

    public int execProcScalar(String procName, String[] params) throws ClassNotFoundException, SQLException {
        CallableStatement cs = this.getConnection().prepareCall(procName);
        for (int i = 0; i < params.length; i++) {
            cs.setString(i + 1, params[i]);
        }
        return cs.executeUpdate();
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Contructor">
    //contructor
    public DataSource() {
        cf = new AppConfig();

//        try {
//            loadConfig();
//        } catch (IOException ex) {
//            defaultConfig();
//        }
    }
    // </editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Feild">
    AppConfig cf;

    public AppConfig getCf() {
        return cf;
    }

    public void setCf(AppConfig cf) {
        this.cf = cf;
    }
    //</editor-fold>

    //    // <editor-fold defaultstate="collapsed" desc="App config method">
    //Feild 
//    private File f = new File("db.properties");
//    private String dbServer;
//    private String dbPort;
//    private String dbType;
//    private String dbDriver;
//    private String dbName;
//    private String dbUser;
//    private String dbPassword;
//
//    //get set
//    public String getDbPort() {
//        return dbPort;
//    }
//
//    public void setDbPort(String dbPort) {
//        this.dbPort = dbPort;
//    }
//
//    public File getF() {
//        return f;
//    }
//
//    public void setF(File f) {
//        this.f = f;
//    }
//
//    public String getDbServer() {
//        return dbServer;
//    }
//
//    public void setDbServer(String dbServer) {
//        this.dbServer = dbServer;
//    }
//
//    public String getDbType() {
//        return dbType;
//    }
//
//    public void setDbType(String dbType) {
//        this.dbType = dbType;
//    }
//
//    public String getDbDriver() {
//        return dbDriver;
//    }
//
//    public void setDbDriver(String dbDriver) {
//        this.dbDriver = dbDriver;
//    }
//
//    public String getDbName() {
//        return dbName;
//    }
//
//    public void setDbName(String dbName) {
//        this.dbName = dbName;
//    }
//
//    public String getDbUser() {
//        return dbUser;
//    }
//
//    public void setDbUser(String dbUser) {
//        this.dbUser = dbUser;
//    }
//
//    public String getDbPassword() {
//        return dbPassword;
//    }
//
//    public void setDbPassword(String dbPassword) {
//        this.dbPassword = dbPassword;
//    }
//    public boolean checkFileConfigExist() {
//        if (f.exists()) {
//            return true;
//        }
//        return false;
//    }
//
//    //Load config from default file config properties
//    public void loadConfig() throws IOException {
//        //Create Properties object
//        Properties objProp = new Properties();
//        try (FileInputStream fileInputStream = new FileInputStream(f)) {
//            objProp.load(fileInputStream);
//            //bind config to feild
//            this.dbServer = objProp.getProperty("dbServer");
//            this.dbName = objProp.getProperty("dbName");
//            this.dbUser = objProp.getProperty("dbUser");
//            this.dbPassword = objProp.getProperty("dbPassword");
//            this.dbType = objProp.getProperty("dbType");
//            this.dbDriver = objProp.getProperty("dbDriver");
//            this.dbPort = objProp.getProperty("dbPort");
//        }
//
//    }
//
//    //Load config from specify config.properties file
//    public void loadConfig(File file) throws IOException {
//        //Create Properties object
//        Properties objProp = new Properties();
//        //load config from file 
//        objProp.load(new FileInputStream(file));
//        //bind config to feild
//        this.dbServer = objProp.getProperty("dbServer");
//        this.dbName = objProp.getProperty("dbName");
//        this.dbUser = objProp.getProperty("dbUser");
//        this.dbPassword = objProp.getProperty("dbPassword");
//        this.dbType = objProp.getProperty("dbType");
//        this.dbDriver = objProp.getProperty("dbDriver");
//        this.dbPort = objProp.getProperty("dbPort");
//    }
//
//    //Save current config
//    /**
//     * Save current configuration to properties file
//     *
//     * @throws java.io.FileNotFoundException
//     */
//    public void saveConfig() throws FileNotFoundException, IOException {
//        //Create Properties object
//        Properties objProp = new Properties();
//        objProp.setProperty("dbServer", this.dbServer);
//        objProp.setProperty("dbName", this.dbName);
//        objProp.setProperty("dbUser", this.dbUser);
//        objProp.setProperty("dbPassword", this.dbPassword);
//        objProp.setProperty("dbType", this.dbType);
//        objProp.setProperty("dbDriver", this.dbDriver);
//        objProp.setProperty("dbPort", this.dbPort);
//        objProp.store(new FileOutputStream("db.properties"), "database config");
//
//    }
//
//    //Export config to file
//    public void saveConfig(File f) throws IOException {
//        //Create Properties object
//        Properties objProp = new Properties();
//        objProp.setProperty("dbServer", this.dbServer);
//        objProp.setProperty("dbName", this.dbName);
//        objProp.setProperty("dbUser", this.dbUser);
//        objProp.setProperty("dbPassword", this.dbPassword);
//        objProp.setProperty("dbType", this.dbType);
//        objProp.setProperty("dbDriver", this.dbDriver);
//        objProp.setProperty("dbPort", this.dbPort);
//        objProp.store(new FileOutputStream(f), "database config");
//    }
//
//    //Default config
//    private void defaultConfig() {
//        this.dbServer = "127.0.0.1";
//        this.dbPort = "1433";
//        this.dbType = "sqlserver";
//        this.dbDriver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
//        this.dbName = "";
//        this.dbUser = "";
//        this.dbPassword = "";
//    }
//// </editor-fold>
}

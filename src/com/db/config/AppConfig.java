package com.db.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ThucNV
 */
public class AppConfig {
    private File f = new File("db.properties");
    private String dbHost;
    private String dbPort;
    private String dbType;
    private String dbDriver;
    private String dbName;
    private String dbUser;
    private String dbPassword;

    public AppConfig() {
        try {
            loadConfig();
        } catch (IOException ex) {
            defaultConfig();
            try {
                saveConfig();
            } catch (IOException ex1) {
                Logger.getLogger(AppConfig.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    //<editor-fold defaultstate="collapsed" desc=" Getter and Setter ">
    public String getdbHost() {
        return dbHost;
    }

    public String getDbType() {
        return dbType;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public String getDbDriver() {
        return dbDriver;
    }

    public void setDbDriver(String dbDriver) {
        this.dbDriver = dbDriver;
    }

    public void setdbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public String getPort() {
        return dbPort;
    }

    public void setPort(String Port) {
        this.dbPort = Port;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDbUser() {
        return dbUser;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public String getDbPassword() {
        return dbPassword;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public File getF() {
        return f;
    }

    public void setF(File f) {
        this.f = f;
    }

    //</editor-fold>
    //check file config exist 
    public boolean checkFileConfigExist() {
        if (f != null) {
            return f.exists();
        }
        return false;
    }

    //Load config from default file config properties
    public void loadConfig() throws IOException {
        //Create Properties object
        Properties objProp = new Properties();
        try (FileInputStream fileInputStream = new FileInputStream(f)) {
            objProp.load(fileInputStream);
            //bind config to feild
            this.dbHost = objProp.getProperty("dbHost");
            this.dbPort = objProp.getProperty("dbPort");
            this.dbName = objProp.getProperty("dbName");
            this.dbUser = objProp.getProperty("dbUser");
            this.dbPassword = objProp.getProperty("dbPassword");
            this.dbType = objProp.getProperty("dbType");
            this.dbDriver = objProp.getProperty("dbDriver");
        }

    }

    //Load config from specify config.properties file
    public void loadConfig(File file) throws IOException {
        //Create Properties object
        Properties objProp = new Properties();
        //load config from file 
        objProp.load(new FileInputStream(file));
        //bind config to feild
        this.dbHost = objProp.getProperty("dbHost");
        this.dbPort = objProp.getProperty("dbPort");
        this.dbName = objProp.getProperty("dbName");
        this.dbUser = objProp.getProperty("dbUser");
        this.dbPassword = objProp.getProperty("dbPassword");
        this.dbType = objProp.getProperty("dbType");
        this.dbDriver = objProp.getProperty("dbDriver");
    }

    //Save current config
    public void saveConfig() throws IOException {
        //Create Properties object
        Properties objProp = new Properties();
        objProp.setProperty("dbHost", this.dbHost);
        objProp.setProperty("dbPort", this.dbPort);
        objProp.setProperty("dbName", this.dbName);
        objProp.setProperty("dbUser", this.dbUser);
        objProp.setProperty("dbPassword", this.dbPassword);
        objProp.setProperty("dbType", this.dbType);
        objProp.setProperty("dbDriver", this.dbDriver);
        objProp.store(new FileOutputStream("db.properties"), "Database server config.\n Use SERVER\\INSTANCE if your database in a instance of server");
    }

    //Export config to file
    public void saveConfig(File f) throws IOException {
        //Create Properties object
        Properties objProp = new Properties();
        objProp.setProperty("dbHost", this.dbHost);
        objProp.setProperty("dbPort", this.dbPort);
        objProp.setProperty("dbName", this.dbName);
        objProp.setProperty("dbUser", this.dbUser);
        objProp.setProperty("dbPassword", this.dbPassword);
        objProp.setProperty("dbType", this.dbType);
        objProp.setProperty("dbDriver", this.dbDriver);
        objProp.store(new FileOutputStream(f), "Database server config.\n Use SERVER\\INSTANCE if your database in a instance of server");
    }

    //Default config
    public void defaultConfig() {
        this.dbHost = "localhost";
        this.dbPort = "1433";
        this.dbType = "sqlserver";
        this.dbDriver = "com.microsoft.sqlserver.jdbc.SQLdbHostDriver";
        this.dbName = "JavaDemo";
        this.dbUser = "sa";
        this.dbPassword = "1234$";
    }
}
